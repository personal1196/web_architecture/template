FROM debian:stable-slim
COPY target/release/TODO .
COPY config.yml .
RUN apt-get update && apt-get -y upgrade && apt-get install -y libssl-dev
CMD ["./TODO"]
